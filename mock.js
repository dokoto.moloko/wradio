#!/usr/bin/env node

/* eslint-disable no-console */
const fetch = require('node-fetch');
const get = require('lodash/get');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const users = [];
const API_HOST = 'http://radio.garden/api';
const app = express();
const jsonParser = bodyParser.json();
const PORT = 5050;

function getArg(key, fallback = '') {
  const index = process.argv.findIndex(k => k === key);
  if (index === -1) return fallback;
  if (index + 1 >= process.argv.length) return fallback;
  return process.argv[index + 1];
}

const port = getArg('-p', PORT);

app.use(cors());
const server = app.listen(port, () => {
  console.log(`Local server started at localhost:${port}`);
  console.log('CTRL + c to exit ;)');
});

process.on('SIGINT', () => {
  console.log('CTRL + c (SIGINT) detected. Closing server');
  server.close();
});

process.on('uncaughtException', () => {
  console.log('Uncaught exception. Closing server');
  server.close();
});

process.on('SIGTSTP', () => {
  console.log('CTRL + z (SIGTSTP) detected. Closing server');
  server.close();
});

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

function isAuth(req) {
  const Authorization = req.header('Authorization');
  return !!Authorization;
}

function getUserByToken(req) {
  const token = req.headers.authorization.split(' ')[1];
  return users.find(user => user.token === token);
}

app.post('/oauth/token', jsonParser, (req, res) => {
  const { username, password } = req.body;
  const user = users.find(user => user.username === username && user.password === password);
  if (user) {
    res.json({
      access_token: user.token,
      session: user,
    });
  } else {
    res.send(401, 'invalid user/password');
  }
});

app.get('/search', async (req, res) => {
  try {
    const query = get(req, 'query.q', '');
    const freq = await fetch(`${API_HOST}/search?q=${query}`);
    const stations = await freq.json();
    res.json(stations);
  } catch (error) {
    console.error(error);
    res.send(500);
  }
});

/*
app.get('/users-profile/operations', (req, res) => {
  if (isAuth(req)) {
    const user = getUserByToken(req);
    const operation = usesrOperationsDb
      .filter(userOps => {
        const opsOK = userOps.id === user.operationId;
        const locationOK = userOps.locations.some(loc => loc.id === user.locationId);
        return opsOK && locationOK;
      });
    res.json(operation);
  } else {
    res.send(401, 'invalid token...');
  }
});

// orders/search?location_id=xxxx-xxxx-xxxx-xxxx&search=xxxxx
app.get('/orders/search', (req, res) => {
  if (isAuth(req)) {
    const refBar = DatasetOrderReferenceBarcode.find(bc => bc.barcode === req.query.search);
    const reference = get(refBar, 'reference', null);
    if (reference) {
      const order = orderDb.find(or => or.reference === reference);
      if (order) {
        order.locations = order.locations.filter(lo => lo.id === req.query.location_id);
        res.json(order);
      } else {
        res.status(404).json({ error: `${req.query.search} not found`, search: req.query.search });
      }
    } else {
      res.status(404).json({ error: `${req.query.search} not found`, search: req.query.search });
    }
  } else {
    res.send(401, 'invalid token...');
  }
});

// orders/{order_reference}?location_id
app.get('/orders/:order_reference', (req, res) => {
  if (isAuth(req)) {
    const orderDetail = orderDetailDb.find(od => od.reference === req.params.order_reference);
    orderDetail.articles = orderDetail.articles.filter(article => article.location.id === req.query.location_id);
    res.json(orderDetail);
  } else {
    res.send(401, 'invalid token...');
  }
});

// orders?location_id=xxxx-xxxx-xxxx-xxxxx
app.get('/orders', (req, res) => {
  if (isAuth(req)) {
    const respJSON = orderDb.filter(d => d.locations.some(l => l.id === req.query.location_id));
    res.json(respJSON);
  } else {
    res.send(401, 'invalid token...');
  }
});

// orders/{order_reference}/articles/check?location_id=xxxx-xxxx-xxxx-xxxx&reference=xxxxxxxxxx&confirm=true
// patch
app.patch('/orders/:order_reference/articles/check', jsonParser, (req, res) => {
  if (isAuth(req)) {
    console.log(`Params ${JSON.stringify(req.params)} ${JSON.stringify(req.query)}`);
    const { locations } = orderDb.find(ord => ord.reference === req.params.order_reference);
    console.log(`Locations found ${JSON.stringify(locations)}`);
    const myLocation = locations.find(loc => loc.id === req.query.location_id);
    console.log(`My Location found ${JSON.stringify(myLocation)}`);
    const { articles } = orderDetailDb.find(od => od.reference === req.params.order_reference);
    console.log(`Articles by reference ${JSON.stringify(articles)}`);
    let article = null;
    if (articles.some(ars => ars.reference === req.query.reference)) {
      article = articles.find(ars => ars.reference === req.query.reference);
      console.log(`Article found on articles ${article.reference}`);
    } else {
      const articleBarcode = DatasetArticleReferenceBarcode.find(ars => ars.barcode === req.query.reference);
      console.log(`ArticleReference found on barcodes ${JSON.stringify(articleBarcode)}`);
      const articleReference = get(articleBarcode, 'articleReference');
      article = articles.find(ars => ars.reference === articleReference);
      console.log(`Article found on articles ${JSON.stringify(article)}`);
    }

    if (!article) {
      console.log(`404 Article not found ${JSON.stringify(article)}`);
      res.send(404);
    } else if (article.packages_count === 1) {
      myLocation.articles_checked_count = myLocation.articles_checked_count < myLocation.articles_count
        ? myLocation.articles_checked_count + 1
        : myLocation.articles_checked_count;
      article.checked = true;
      console.log(`200 Article checked ${JSON.stringify(myLocation)}`);
      res.send(200);
    } else if (article.packages_count > 1 && req.query.confirm === 'true') {
      myLocation.articles_checked_count = myLocation.articles_checked_count < myLocation.articles_count
        ? myLocation.articles_checked_count + 1
        : myLocation.articles_checked_count;
      article.checked = true;
      console.log(`200 Article re-verified ${JSON.stringify(myLocation)}`);
      res.send(200);
    } else if (article.packages_count > 1) {
      console.log(`202 Article needs to be verified ${JSON.stringify(article)}`);
      res.status(202).json({
        packages_count: article.packages_count,
      });
    }
  } else {
    res.send(401, 'invalid token...');
  }
});
*/
