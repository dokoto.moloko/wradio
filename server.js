#!/usr/bin/env node


const express = require('express');
const cors = require('cors');
const history = require('connect-history-api-fallback');

const PORT = 3030;

function getArg(key, fallback = '') {
  const index = process.argv.findIndex(k => k === key);
  if (index === -1) return fallback;
  if (index + 1 >= process.argv.length) return fallback;
  return process.argv[index + 1];
}

const port = getArg('-p', PORT);
const directory = getArg('-d', 'dist');
const app = express();
app.use(history());
app.use(express.static(directory));
app.use(cors());
app.get('/', (req, res) => {
  res.sendFile(`${directory}/index.html`);
});

const server = app.listen(port, () => { 
  console.log(`Local server started at localhost:${port} serving on ${directory}`);
  console.log('CTRL + c to exit ;)');
});

process.on('SIGINT', () => {
  console.log('CTRL + c (SIGINT) detected. Closing server');
  server.close();
});

process.on('uncaughtException', () => {
  console.log('Uncaught exception. Closing server');
  server.close();
});

process.on('SIGTSTP', () => {
  console.log('CTRL + z (SIGTSTP) detected. Closing server');
  server.close();
});
