import Cookies from 'js-cookie';
import set from 'lodash/set';
import factory from '@goi-pub/areq';
import router from '@/router';
import store from '@/store';

const resolveNoAuth = () => {
  store.dispatch('auth/logout')
    .then(() => router.push({ name: 'login' }))
    .catch(() => {});
};

const session = Cookies.getJSON('session');
const core = {
  baseUrl: process.env.VUE_APP_API_HOST,
  ...session,
  resolveNoAuth,
};

export default {
  install: function (Vue) {
    const areq = factory(core);
    set(Vue, 'areq', areq);
    set(Vue, 'prototype.$areq', areq);
  },
};
