import { API_HOST } from '../../constants';

export async function indexRadios(queryParams) {
  const req = await fetch(`${API_HOST}/${queryParams}`,
    {
      mode: 'cors',
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    });
  return req.json();
}
