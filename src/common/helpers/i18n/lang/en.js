export default {
  app: {
    'not-found_title': 'Error 404',
    'not-found_upper-title': 'Ups! This page doesn’t exist.',
    'not-found_subtitle': 'Maybe the url is wrong, maybe there is technical error. If you didn’t mistyped anything, please try again and if the error persist you can contact us at',
    'not-found_email': 'support@letsgoi.com',
    'not-results_title': 'Ouch! There are no results for this search.',
    'not-results_subtitle': 'Please, check your spell and try again.',
    'form-errors': 'A field is incorrect! | {totalerrors} fields are incorrects!',
  },
};
