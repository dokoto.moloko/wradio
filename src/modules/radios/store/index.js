import get from 'lodash/get';
import { indexRadios } from '../../../common/helpers/api/services';
import { stations as store } from '../../../store';

export async function doIndexRadios(text) {
  try {
    const radios = await indexRadios(`search?q=${text}`);
    const favorites = get(radios, 'hits.hits', [])
      .map(radio => get(radio, '_source', { code: '', placeId: '', title: '', type: '', url: '' }));
    store.set(favorites);
  } catch (error) {
    console.error(error);
  }
}
