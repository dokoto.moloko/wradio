import { writable } from 'svelte/store';

export const router = writable({
	module: null,
	params: {},
});

export const stations = writable([]);
export const myStations = writable([]);
export const settings = writable({});
