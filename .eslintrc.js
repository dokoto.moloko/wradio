const path = require('path');

module.exports = {
  env: {
    es6: true,
    browser: true,
  },
  plugins: [
    'svelte3',
  ],
  parserOptions: {
    ecmaVersion: 2021,
    sourceType: 'module'
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['@', path.join(__dirname, './src')],
          ['@radios', path.join(__dirname, './src/modules/radios')],
        ],
        extensions: ['.ts', '.js', '.jsx', '.json', 'svelte'],
      },
    },
  },
  overrides: [
    {
      files: ['*.svelte'],
      processor: 'svelte3/svelte3'
    }
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-shadow': 0,
    'arrow-parens': 0,
    'object-shorthand': 0,
    'func-names': 0,
    'import/prefer-default-export': 0,
    'object-curly-newline': 0,
    'import/no-extraneous-dependencies': 0,
    'space-before-function-paren': 0,
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: ['currState'],
      },
    ],
    'no-multiple-empty-lines': ["error", { "max": 2, "maxEOF": 0 }],
    'no-useless-catch': 0,
  },
};
